# Lab 1: VM Setup

1. Find your group. You can find your group under Canvas - People - Study Group. Ask your TA/peer mentor if you need help finding your group members.

2. Get to know your group members, asking each other the following:

- Name + major
- What is your hometown, and what is your favorite place there?
- If you could create a silly law, what would it be?

3. Setup your [virtual machine](./vm)
